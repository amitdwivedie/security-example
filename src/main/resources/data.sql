insert into users(username, firstname, lastname, password, email, enabled) 
			values 	('amit', 'Amit', 'Dwivedi', '{bcrypt}$2a$10$J5.gkU/rBZwvsJiOq83b8O0r1uto5fkNfrFAzOdt1MxKHQOhQtFGq', 'amit@localhost', true),
				   	('admin', 'Admin', 'User', '{bcrypt}$2a$10$2KKeXYsRLCN.oxVGkKul6ulN5Rlc7HXYPNyIisPSw6Xf63ZymhRje', 'admin@localhost', true),
				   	('user', 'User', '', '{bcrypt}$2a$10$MF1a.3DY5Y/Hvb3ZxkUQcuvOcCmKfCi3Yz3IEo4rfJ6kI.UGJw/7K', 'user@localhost', true),
				   	('system', 'System', 'User', '{bcrypt}$2a$10$mZ7IadhSW/WAL8/gA7U2CuzM5PHIKgutSwKhh2bzOYx.0i2IRV64O', 'system@localhost', true),
				   	('test', 'Test', 'User', '{bcrypt}$2a$10$cZWt9sDVMk70j1E336YwkuG5xTIRQY2OVu1mwCZI0bs7ejiUjLEye', 'test@localhost', true);
				   
insert into authorities(name) values ('ROLE_USER'), ('ROLE_ADMIN'), ('ROLE_TEST');

insert into user_authority(username, authority)
			values 	('amit', 'ROLE_USER'),
					('amit', 'ROLE_ADMIN'),
					('admin', 'ROLE_USER'),
					('admin', 'ROLE_ADMIN'),
					('user', 'ROLE_USER'),
					('system', 'ROLE_USER'),
					('test', 'ROLE_TEST');
				   
					