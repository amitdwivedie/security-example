package com.amit.ms.sercurity.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.amit.ms.sercurity.domain.User;


public interface UserRepository extends JpaRepository<User, String>{

	User findByUsername(String username);
	
}
