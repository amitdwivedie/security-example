package com.amit.ms.sercurity.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.amit.ms.sercurity.domain.Authority;


public interface AuthorityRepository extends JpaRepository<Authority, String>{
	
}
