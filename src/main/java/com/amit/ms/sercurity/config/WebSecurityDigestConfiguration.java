package com.amit.ms.sercurity.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.www.DigestAuthenticationEntryPoint;
import org.springframework.security.web.authentication.www.DigestAuthenticationFilter;

@Configuration
@Order(1)
public class WebSecurityDigestConfiguration extends WebSecurityConfigurerAdapter{

	private static final String REALM_NAME = "digest_testing";
	private static final String NONCE_KEY = "digest_secret_key";
	private static final String DIGEST = "digest";
	private static final String DIGEST1 = "digest1";
	private static final String PASSWORD = "{bcrypt}$2a$10$WEhxSHGQRb75.HSrbkF5IejA65daQZHANeJ5cJ.C4wwnMkEqfzFyO";
	private static final String ROLE_ADMIN = "ADMIN";
	private static final String ROLE_USER = "USER";
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	/**
	 * Configure http security with digest filter and digest authentication
	 */
	protected void configure(HttpSecurity http) throws Exception {
		http.antMatcher("/digest")
			.addFilter(digestAuthenticationFilter())
			.exceptionHandling()
			.authenticationEntryPoint(getDigestEntrypoint())
			.and()
			.authorizeRequests()
			.antMatchers("/digest")
			.hasRole(ROLE_ADMIN);
	}
	
	
	/**
	 * Configure digest authentication filter
	 */
	private DigestAuthenticationFilter digestAuthenticationFilter() throws Exception {
		DigestAuthenticationFilter filter = new DigestAuthenticationFilter();
		filter.setUserDetailsService(userDetailsServiceBean());
		filter.setAuthenticationEntryPoint(getDigestEntrypoint());
		return filter;
	}

	
	/**
	 * Add digest entry point
	 */
	private DigestAuthenticationEntryPoint getDigestEntrypoint() {
		DigestAuthenticationEntryPoint entryPoint = new DigestAuthenticationEntryPoint();
		entryPoint.setRealmName(REALM_NAME);
		entryPoint.setKey(NONCE_KEY);
		return entryPoint;
	}
	
	/**
	 * Configure in memory users with role
	 */
	protected void configure(AuthenticationManagerBuilder authBuilder) throws Exception {
		authBuilder.inMemoryAuthentication()
		.passwordEncoder(passwordEncoder)
		.withUser(DIGEST1).password(PASSWORD).roles(ROLE_USER)
		.and()
		.withUser(DIGEST).password(PASSWORD).roles(ROLE_ADMIN);
	}

}
