package com.amit.ms.sercurity.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.client.web.OAuth2AuthorizedClientRepository;
import org.springframework.security.oauth2.client.web.reactive.function.client.ServletOAuth2AuthorizedClientExchangeFilterFunction;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class WebClientConfig {

	@Bean
	public WebClient webClient(ClientRegistrationRepository registrationRepository,
			OAuth2AuthorizedClientRepository authorizedClientRepository) {

		ServletOAuth2AuthorizedClientExchangeFilterFunction oauth2 = new ServletOAuth2AuthorizedClientExchangeFilterFunction(
				registrationRepository, authorizedClientRepository);
		oauth2.setDefaultClientRegistrationId("web_client");

		return WebClient.builder().apply(oauth2.oauth2Configuration()).build();
	}
}
