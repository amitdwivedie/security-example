package com.amit.ms.sercurity.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DigestResource {

	@GetMapping("/digest")
	public String digest() {
		return "Digest Testing!!! ";
	}
}
