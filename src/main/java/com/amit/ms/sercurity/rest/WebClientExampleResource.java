package com.amit.ms.sercurity.rest;

import java.net.URI;
import java.net.URISyntaxException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;

@RestController
public class WebClientExampleResource {

	@Autowired
	private WebClient webClient;

	@GetMapping("/webClientTest")
	public String example() {
		String response = null;
		try {
			response = webClient.get().uri(new URI("http://localhost:7676/auth/check")).retrieve().bodyToMono(String.class)
					.block();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		return response;
	}
}
