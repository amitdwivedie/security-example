package com.amit.ms.sercurity.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloResource {

	@GetMapping("/hello")
	public String hello(@RequestParam(required = false, defaultValue = "Amit") String name ) {
		return "Hello " + name;
	}
}
