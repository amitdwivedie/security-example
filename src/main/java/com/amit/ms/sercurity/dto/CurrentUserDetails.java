package com.amit.ms.sercurity.dto;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

public class CurrentUserDetails extends User {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1917181715491168768L;

	private String email;

	public CurrentUserDetails(String username, String password, String email,
			Collection<? extends GrantedAuthority> authorities) {
		super(username, password, authorities);
		this.email = email;
	}

	public String getEmail() {
		return email;
	}
}
